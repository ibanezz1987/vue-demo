const formData = {
    formModel: {
        name: '',
        mail: '',
        sex: '',
        times: [],
        device: '',
        comments: '',
    },
    formFields: [
        {group: 'default', type: "text", name: "name", title: 'Naam', required: true},
        {group: 'default', type: "email", name: "mail", title: 'E-mailadres', required: true},
        {group: 'multiple', type: "radio", name: "sex", title: 'Geslacht', required: true, children: [
            {value:'male', title: 'Man'},
            {value:'female', title: 'Vrouw'},
        ]},
        {group: 'multiple', type: "checkbox", name: "times", title: 'Werktijden', children: [
            {value:'morning', title: 'Ochtend'},
            {value:'midday', title: 'Middag'},
            {value:'evening', title: 'Avond'},
        ]},
        {group: 'select', type: "select", name: "device", title: 'Device', children: [
            {value:'phone', title: 'Mobiel'},
            {value:'tablet', title: 'Tablet'},
            {value:'desktop', title: 'Desktop'},
        ]},
        {group: 'textarea', type: "textarea", name: "comments", title: 'Opmerkingen'},
    ],
};
export default formData;
