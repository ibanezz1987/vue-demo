import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Filters
import '@/filters/currency'
import '@/filters/uppercase'
import '@/filters/trim'

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
