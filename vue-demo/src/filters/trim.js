import Vue from 'vue';

Vue.filter('trim', function (value, length) {
    if (!value) return;
    return value.substr(0, length);
});
