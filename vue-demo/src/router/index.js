import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
    },
    {
        path: '/directives',
        name: 'directives',
        component: () => import(/* webpackChunkName: "directives" */ '../views/Directives.vue')
    },
    {
        path: '/forms',
        name: 'forms',
        component: () => import(/* webpackChunkName: "forms" */ '../views/Forms.vue')
    },
    {
        path: '/forms-verzonden',
        name: 'forms-verzonden',
        component: () => import(/* webpackChunkName: "formSubmitted" */ '../views/FormSubmitted.vue')
    },
    {
        path: '/signin',
        name: 'signin',
        component: () => import(/* webpackChunkName: "signin" */ '../views/SignIn.vue')
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
