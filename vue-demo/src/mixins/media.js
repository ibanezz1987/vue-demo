export default {
    methods: {
        getImageUrl(fileName) {
            return require('../assets/' + fileName);
        },
        getFlagUrl(img) {
            return require('../assets/countries/' + img);
        },
    },
}
