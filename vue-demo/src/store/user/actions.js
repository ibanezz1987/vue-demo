export default {
    setUser({ commit }, user) {
        commit('SET_USER', user)
    },
    unsetUser({ commit }) {
        commit('UNSET_USER')
    },
}
