# Vue Demo

Code snippers for vue.js

## Vereisten
- Node.js
- Vue CLI
- Chrome Vue Devtools

## Vue CLI
- Installatie: `npm install -g @vue/cli`
- Versie: `vue --version`
- Nieuw: `vue create my-app`
- Vue serve: `cd my-app` + `npm run serve`
- Visuele versie: `vue ui`

## main.js
- Dit geeft een waarschuwing over de development/productieversie van Vue `Vue.config.productionTip = false`

## Vue router
- Router met of zonder `/#/`
- `const router = new VueRouter({mode: 'history'})`
- `mode: 'history'` zorgt ervoor dat `/#/` weg gaat
- Server moet dit echter wel ondersteunen